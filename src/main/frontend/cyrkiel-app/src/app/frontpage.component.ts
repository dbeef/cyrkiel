import {Component, Injectable, OnInit} from '@angular/core';
import {fadeInAnimation} from './fade-in.animation';
import {Router, NavigationEnd} from '@angular/router';
import {Http, HttpModule} from "@angular/http";
import {Observable} from "rxjs/Observable";


class Info {
  liczbaZadan: number;
}

@Injectable()
export class CounterService {
  constructor(private http: Http) {
  }

  counter = 0;

  count() {
    let promise = new Promise((resolve, reject) => {
      let countUrl = 'http://localhost:8888/zadanie/count';
      this.http.get(countUrl)
        .toPromise()
        .then(
          res => { // Success
            console.log(res.json().liczbaZadan);
            this.counter = res.json().liczbaZadan;
            resolve();
          },
          msg => { // Error
            reject(msg);
          }
        );
    });
    return promise;
  }

}

@Component({
  templateUrl: './frontpage.component.html',

  // make fade in animation available to this component
  animations: [fadeInAnimation],

  // attach the fade in animation to the host (root) element of this component
  host: {'[@fadeInAnimation]': ''},

  providers: [CounterService],
  moduleId: module.id.toString()
})
export class FrontpageComponent implements OnInit {
  constructor(router: Router, private _service: CounterService,) {
    this.router = router;
  }

  model: any;
  router: Router;
  searching = false;
  searchFailed = false;
  loadingCounter = false;
  counter = 0;
  hideSearchingWhenUnsubscribed = new Observable(() => () => this.searching = false);

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });

    this.loadingCounter = true;
    this._service.count().then(() => this.onfulfilled());

  }

  onfulfilled(): void {
    this.loadingCounter = false
    this.counter = this._service.counter;
  }

  formatter(): string {

    //Pattern:
    //rozwiązanie 1
    //rozwiązania 2, 3, 4, 22, 23, 24, 32, 33, 34, ...
    //rozwiązań 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 25, 26, 27, 28, 29, 30, 31, 35...

    var counterAsString = this.counter.toLocaleString();
    var length = counterAsString.length;
    var lastChar = counterAsString.charAt(length - 1);

    if (length == 1 && lastChar === "1")
      return " rozwiązanie.";
    else {
      if(lastChar === "2" || lastChar == "3" || lastChar == "4")
        return " rozwiązania.";
      else
        return " rozwiązań.";
    }
  }
}
