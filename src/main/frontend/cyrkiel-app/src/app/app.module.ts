import{BrowserModule}from'@angular/platform-browser';
import {NgModule }from '@angular/core';
import {FormsModule, ReactiveFormsModule}from '@angular/forms';
import {AppComponent}from './app.component';
import { DogListComponent }      from './dog-list.component';
import { CatListComponent }   from './cat-list.component';
import { FrontpageComponent }   from './frontpage.component';
import {NgbdTypeaheadHttp}from './typehead.template';
import {NgbModule}from '@ng-bootstrap/ng-bootstrap';
import { routing } from './app.routes';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeroListBasicComponent } from './hero-list-basic.component';
import { MathJaxRenderer } from './mathjax-renderer.directive';
import {HttpClientModule} from '@angular/common/http';
import {HttpModule} from '@angular/http';
import {JsonpModule } from '@angular/http';
import {MessageService} from "./message.service";

@NgModule({
  declarations: [
    AppComponent,
    NgbdTypeaheadHttp,
DogListComponent,
CatListComponent,
FrontpageComponent, HeroListBasicComponent, MathJaxRenderer
  ],
  imports: [
    BrowserModule,BrowserAnimationsModule, JsonpModule,FormsModule, NgbModule.forRoot(), routing, HttpClientModule, HttpModule],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
