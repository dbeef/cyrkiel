import {Directive, ElementRef, Input} from '@angular/core';
import {Observable} from 'rxjs/Rx';

declare var MathJax: any;

@Directive({
  selector: '[MathJax]'
})

export class MathJaxRenderer {

  @Input('MathJax') fractionString: string;

  constructor(private el: ElementRef) {
  }


  ngDoCheck() {
    console.log("Check!");

  }

  /*
  function delay(ms: number) {
      return new Promise(resolve => setTimeout(resolve, ms));
  }
  */


  ngOnChanges() {
    console.log('>> ngOnChanges');
    console.log(MathJax);
    setTimeout(() => MathJax.Hub.Queue(["Typeset", MathJax.Hub]), 2000);
    this.el.nativeElement.innerHTML = this.fractionString;

    MathJax.Hub.Queue(["Typeset", MathJax.Hub, this.el.nativeElement]);
  }

}

//    MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
