// ====== ./app/app.routes.ts ======

// Imports
// Deprecated import
// import { provideRouter, RouterConfig } from '@angular/router';
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatListComponent } from './cat-list.component';
import { DogListComponent } from './dog-list.component';
import { FrontpageComponent } from './frontpage.component';

// Route Configuration
export const routes: Routes = [
{ path: 'cats', component: CatListComponent },
{ path: 'dogs', component: DogListComponent },
{ path: '', component: FrontpageComponent }
];

// Deprecated provide
// export const APP_ROUTER_PROVIDERS = [
//   provideRouter(routes)
// ];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
