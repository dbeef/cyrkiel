export class Zadanie {

  constructor(img: string, ksiazka: string, numer: string, tresc: string, rozwiazanie: string) {
    this.img = img;
    this.ksiazka = ksiazka;
    this.numer = numer;
    this.tresc = tresc;
    this.rozwiazanie = rozwiazanie;
  }
  img: string;
  ksiazka: string;
  numer: string;
  tresc: string;
  rozwiazanie: string;

}
