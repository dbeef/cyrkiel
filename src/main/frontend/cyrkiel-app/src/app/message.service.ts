import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import {Zadanie} from "./zadanie";

@Injectable()
export class MessageService {
  private subject = new Subject<any>();
  private zadanie: Zadanie;

  sendMessage(message: Zadanie) {
    console.log("Sending message!");
    console.log(message);
    this.subject.next({ text: message });
  }

  setZadanie(t: Zadanie){ this.zadanie = t};
  getZadanie(): Zadanie{ return this.zadanie};

  clearMessage() {
    this.subject.next();
  }

  getMessage(): Observable<any> {
    console.log("GET MESSAGE CALLED");
    return this.subject.asObservable();
  }
}
