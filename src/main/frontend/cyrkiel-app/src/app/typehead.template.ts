import {Component, Injectable} from '@angular/core';
import {Jsonp, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/merge';
import {Zadanie} from "./zadanie";
import {Router}from '@angular/router';
import {MessageService} from "./message.service";

declare var MathJax: any;

@Injectable()
export class WikipediaService {
  constructor(private _jsonp: Jsonp) {}

  search(term: string) {
    if (term === '') {
      return Observable.of([]);
    }

    let wikiUrl = 'http://localhost:8888/zadanie/find';
    let params = new URLSearchParams();
    params.set('searchphrase', term);
    params.set('format', 'json');
    params.set('callback', 'JSONP_CALLBACK');

    return this._jsonp
      .get(wikiUrl, {search: params})
      .map(response => <Zadanie[]> response.json());
  }
}

@Component({
  selector: 'ngbd-typeahead-http',
  templateUrl: './typehead.template.html',
  providers: [WikipediaService],
  styles: [`.form-control { width: 300px; display: inline; }`]
})
export class NgbdTypeaheadHttp {
  model: any;
  router: Router;
  searching = false;
  searchFailed = false;
  hideSearchingWhenUnsubscribed = new Observable(() => () => this.searching = false);

  constructor(private _service: WikipediaService, router: Router, private messageService: MessageService) {
this.router = router;
  }

  sendMessage(zadanie: Zadanie): void {
    // send message to subscribers via observable subject
    this.messageService.sendMessage(zadanie);
    this.messageService.setZadanie(zadanie);
  }

  clearMessage(): void {
    // clear message
    console.log("Clearing message!");
    this.messageService.clearMessage();
  }

  search = (text$: Observable<string>) =>
    text$
      .debounceTime(300)
      .distinctUntilChanged()
      .do(() => this.searching = true)
      .switchMap(term =>
        this._service.search(term)
          .do(() => this.searchFailed = false)
          .catch(() => {
            this.searchFailed = true;
            return Observable.of([]);
          }))
      .do(() => this.searching = false)
      .merge(this.hideSearchingWhenUnsubscribed);


  formatter = (result: Zadanie) => (result.numer + ' ' +result.ksiazka);
}


/*

getZadania(term: String): Observable<any> {

  var headers1: HttpHeaders = new HttpHeaders();
headers1.set('Access-Control-Allow-Origin', '*');
headers1.set('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE, HEAD');
headers1.set('Access-Control-Allow-Headers', 'Accept, Content-Type, Origin, X-Json, X-Prototype-Version, X-Requested-With" //, "X-My-NonStd-Option');
headers1.set('Access-Control-Allow-Credentials', 'true');
// Make the HTTP request:

return this.http.post('http://localhost:8888/zadanie/find', '1.11', {
  headers: headers1
})*/
