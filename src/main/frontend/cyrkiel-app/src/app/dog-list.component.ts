// ====== ./app/Dogs/dog-list.component.ts ======

// Import component decorator
import {Component, OnInit} from '@angular/core';
import {fadeInAnimation} from './fade-in.animation';
import {Router, NavigationEnd} from '@angular/router';
import {MathJaxRenderer} from './mathjax-renderer.directive';
import {MessageService} from "./message.service";
import {Subscription} from "rxjs/Subscription";
import {Zadanie} from "./zadanie";

declare var MathJax: any;

@Component({
  template: `
    <div class="promo-block">
      <div class="container">
        <div class="row">
          <div class="col-sm-3 sm-margin-b-60">
            <div class="margin-b-30 card" style="text-align: center;">

              <div class="card-header"><p><i>Informacje o źródle</i></p></div>
              <div class="card-title"><p style="margin-top: 20px;">{{getKsiazka()}}</p></div>

              <img src="/assets/img/podreczniki/original/{{getImg()}}" height="256" style="display: block;
    margin: 0 auto; padding: 10px;">

              <p>Indeks zadania:<b>{{getNumer()}}</b></p>
              <!--     <p [MathJax]="fractionString">{{equation}}</p>
              -->
            </div>
          </div>
          <div class="col-sm-9 sm-margin-b-60">
            <div class="margin-b-30 card" style="text-align: center;">

              <div class="card-header"><p><i>Rozwiązanie</i></p></div>
              <div class="card-title"><p></p></div>

              <p [MathJax]="getRozwiazanie()"></p>

              <!--     <p [MathJax]="fractionString">{{equation}}</p>
              -->
            </div>
          </div>
          <!--
                    <div class="col-sm-3">
                      <div class="promo-block-img-wrap">
                        <img class="promo-block-img img-responsive" src="assets/img/machine.png" align="Avatar">
                      </div>
                    </div>-->
        </div>
        <!--// end row -->
      </div>
    </div>
  `,

  moduleId: module.id.toString(),
  // make fade in animation available to this component
  animations: [fadeInAnimation],
  // attach the fade in animation to the host (root) element of this component
  host: {'[@fadeInAnimation]': ''}
})

// Component class
export class DogListComponent implements OnInit {

  //http://docs.mathjax.org/en/latest/start.html#putting-mathematics-in-a-web-page
//The default math delimiters are $$...$$ and \[...\] for displayed mathematics, and \(...\) for in-line mathematics.


  equation: string = `When $a \\\ne 0$, there are ą ę two solutions to \\\(ax^2 + bx + c = 0\\\) and they are
$$x = {-b\\\pm \\\sqrt{b^2-4ac} \\\over 2ą}.$$`;
  equation2: string = ` t_{0} = 15^{00} `;
  /*
  \\ t_{1}=15^{00}-14^{15}=45min=\frac{3}{4}h\\ t_{2}=15^{00}-14^{35}=25min=\frac{25}{60}h\\ s_{c1}=7km\\ s_{c2}=3km\\ \\ Rozw.: \\\\ v_{sr1}=\frac{s_{c1}}{t_{1}}=\frac{7km}{\frac{3}{4}h}=9\frac{1}{3}\frac{km}{h}\approx 9,3\frac{km}{h} \\ v_{sr2}=\frac{s_{c2}}{t_{2}}=\frac{3km}{\frac{25}{60}h}=7,2\frac{km}{h} \\\\*/
//Robić escape dla backslash, dla \ robić \\
  message: Zadanie;
  subscription: Subscription;

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    if (!this.subscription.closed)
      this.subscription.unsubscribe();
  }

  constructor(private router: Router, private messageService: MessageService) {

    this.subscription = this.messageService.getMessage().subscribe(message => {
      console.log("received:");
      console.log(message);
      this.message = message.text;
      this.ngOnInit();
    });

  }

  getRozwiazanie(): string {

    if (this.message && this.message.rozwiazanie) {
          console.log("Returning rozwiazanie");
          console.log(this.message.rozwiazanie);
      return `$$` + this.message.rozwiazanie + `$$`;
    }
    else {
      return `$$` + '' + `$$`;
    }
  }

  getImg(): string {
    if (this.message && this.message.img) {
      console.log("Returning img:");
      console.log(this.message.img);
      console.log("Returning message:");
      console.log(this.message);

      return this.message.img;
    }
    else {
      return '';
    }
  }

  getKsiazka(): string {
    if (this.message && this.message.ksiazka) {
      /*    console.log("Returning ksiazka");
          console.log(this.message.ksiazka);
    */
      return this.message.ksiazka;
    }
    else {
      return '';
    }

  }

  getNumer(): string {
    if (this.message && this.message.numer) {
      /*console.log("Returning numer");
      console.log(this.message.numer);
*/
      return ' ' + this.message.numer;
    }
    else {
      return '';
    }

  }

  ngOnInit() {


    if (!this.message) {
      console.log("before change:")
      console.log(this.message);

      this.message = this.messageService.getZadanie();
      console.log("after change:")
      console.log(this.message);
    }

    console.log("This message is now:");
    console.log(this.message);

    setTimeout(() => MathJax.Hub.Queue(["Typeset", MathJax.Hub]));

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });

  }
}
