module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

copy: {
  main: {
      expand: true,
      src: '../**',
      //Z jakiegos powodu dest to przedostatni podany w sciezce folder
      dest: '../../../../target/classes/static/dest',
    },
   js: {
     expand: true,
         src: '../js/**',
         //Z jakiegos powodu dest to przedostatni podany w sciezce folder
         dest: '../../../../target/classes/static/dest',
   },
       html: {
         expand: true,
             src: '../**.html',
             dest: '../../../../target/classes/static/dest',
       }
},

  watch: {
    js: {
     files: ['../js/**.js'],
     tasks: ['copy:js']
     },
    html: {
     files: ['../**.html'],
         tasks: ['copy:html']
    }
}
  });


  // Load the plugin that provides the "ugli    fy" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');

 grunt.registerTask('default', ['copy']);

};