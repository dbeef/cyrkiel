package pl.com.dbeef.cyrkiel;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;

import java.util.List;

/**
 * Created by tomi on 24.05.17.
 */
@Slf4j
@RestController
@RequestMapping("/zadanie/")
public class ZadanieController {

    @Autowired
    private ZadanieRepository zadanieRepository;

    @RequestMapping(value = "/find", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Zadanie>> find(@RequestParam("searchphrase") String searchphrase) {
        log.info("Searchprhase: {}", searchphrase);
        log.info("List:");

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Access-Control-Allow-Origin", "*");

        return new ResponseEntity<>(zadanieRepository.findZadanieByNumerLikeOrTrescLike(searchphrase, searchphrase), headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Info> count() {
        log.info("Count.");

        Integer counter = zadanieRepository.findAll().size();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Access-Control-Allow-Origin", "*");

        return new ResponseEntity<>(new Info(counter), headers, HttpStatus.OK);
    }


    @ControllerAdvice
    static class JsonpAdvice extends AbstractJsonpResponseBodyAdvice {
        public JsonpAdvice() {
            super("callback");
        }
    }

}