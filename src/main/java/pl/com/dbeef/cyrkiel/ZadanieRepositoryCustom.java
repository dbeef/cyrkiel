package pl.com.dbeef.cyrkiel;

import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.List;

public interface ZadanieRepositoryCustom {

    void setMongoTemplate(MongoTemplate mongoTemplate);

    List<Zadanie> findZadanieByNumerLikeOrTrescLike(String numer, String tresc);
}