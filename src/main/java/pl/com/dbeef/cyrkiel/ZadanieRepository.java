package pl.com.dbeef.cyrkiel;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by tomi on 22.05.17.
 */
@Repository
public interface ZadanieRepository extends MongoRepository<Zadanie, String>, ZadanieRepositoryCustom {

     Zadanie findByNumerAndKsiazka(String numer, String ksiazka);

     List<Zadanie> findByNumer(String numer);
}
