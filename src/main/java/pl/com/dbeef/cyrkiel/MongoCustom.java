package pl.com.dbeef.cyrkiel;

import org.springframework.data.mongodb.core.MongoTemplate;

public interface MongoCustom {

    public void setMongoTemplate(MongoTemplate mongoTemplate);

}
