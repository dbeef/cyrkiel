package pl.com.dbeef.cyrkiel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@XmlRootElement
public class Zadanie {

    String img;
    String ksiazka;
    String numer;
    String tresc;
    String rozwiazanie;
}
