package pl.com.dbeef.cyrkiel;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@Slf4j
public class ZadanieRepositoryImpl implements MongoCustom, ZadanieRepositoryCustom {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<Zadanie> findZadanieByNumerLikeOrTrescLike(String numer, String tresc) {
        log.info("Numer: {} Treść: {}", numer, tresc);
        Query query = new Query();
       // query.addCriteria(new Criteria().orOperator(Criteria.where("numer").regex( numer, "i")));
        query.addCriteria(new Criteria().orOperator(Criteria.where("numer").regex("^" + numer, "i"), Criteria.where("tresc").regex("^" + tresc, "i")));
        log.info("Query: {}", query.getQueryObject().toString());
        return mongoTemplate.find(query, Zadanie.class);
    }
}